package santander.rio.meetup;


import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;
import santander.rio.meetup.dto.CalculoCervezasDTO;
import santander.rio.meetup.dto.ClimaDTO;
import santander.rio.meetup.model.Clima;
import santander.rio.meetup.model.TipoClima;
import santander.rio.meetup.strategy.*;
import santander.rio.meetup.util.Propiedades;

import java.net.URI;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MeetupApplicationTests {

	public static final String QUERY_PARAMETRO_UBICACION = "ubicacion";
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void clima() throws Exception {

		URI uri = UriComponentsBuilder
				.fromHttpUrl("http://localhost:" + this.port)
				.path("/meetup/clima")
				.queryParam(QUERY_PARAMETRO_UBICACION, "cordoba,ar")
				.build()
				.toUri();

		Assert.assertTrue(this
				.restTemplate
				.withBasicAuth("user", "user")
				.getForObject(uri,
						ClimaDTO.class).isOk());
	}

	@Test
	void climaConCredenciales() {
		URI uri = UriComponentsBuilder.fromHttpUrl("http://localhost:" + this.port).path("/meetup/clima").build().toUri();

		ResponseEntity<Map> entity = this
				.restTemplate
				.withBasicAuth("admin", "admin")
				.getForEntity(uri, Map.class);
		Assert.assertEquals( HttpStatus.OK, entity.getStatusCode());
	}

	@Test
	void climaFechaErronea() {
			URI uri = UriComponentsBuilder
					.fromHttpUrl("http://localhost:" + this.port)
					.path("/meetup/clima")
					.queryParam(QUERY_PARAMETRO_UBICACION, "cordoba,ar")
					.queryParam("fecha", "22")
					.build()
					.toUri();

			Assert.assertFalse(this.restTemplate
					.withBasicAuth("user", "user")
					.getForObject(uri,
					ClimaDTO.class)
					.isOk());
	}

	@Test
	void climaSinUbicacion() {
		URI uri = UriComponentsBuilder
				.fromHttpUrl("http://localhost:" + this.port)
				.path("/meetup/clima")
				.build()
				.toUri();

		Assert.assertFalse(this.restTemplate
				.withBasicAuth("user", "user")
				.getForObject(uri,
						ClimaDTO.class)
				.isOk());
	}

	@Test
	void climaSinCredenciales() {
		URI uri = UriComponentsBuilder
				.fromHttpUrl("http://localhost:" + this.port)
				.path("/meetup/clima")
				.build()
				.toUri();
		ResponseEntity<Map> entity = this
				.restTemplate
				.getForEntity(uri, Map.class);
		Assert.assertEquals(HttpStatus.UNAUTHORIZED, entity.getStatusCode());
	}

	@Test
	void calcularCajones() {
		URI uri = UriComponentsBuilder
				.fromHttpUrl("http://localhost:" + this.port)
				.path("/meetup/clima-calculos/cervezas")
				.queryParam(QUERY_PARAMETRO_UBICACION, "cordoba,ar")
				.queryParam("participantes", "4")
				.build()
				.toUri();

		CalculoCervezasDTO dto = this
				.restTemplate
				.withBasicAuth("admin", "admin")
				.getForObject(uri, CalculoCervezasDTO.class);
		Assert.assertTrue(dto.getCajones()>0);
	}

	@Test
	void calcularCajonesSinCredenciales() {
		URI uri = UriComponentsBuilder
				.fromHttpUrl("http://localhost:" + this.port)
				.path("/meetup/clima-calculos/cervezas")
				.queryParam("participantes", "4")
				.build()
				.toUri();

		ResponseEntity<Map> entity = this
				.restTemplate
				.getForEntity(uri, Map.class);
		Assert.assertEquals(HttpStatus.UNAUTHORIZED, entity.getStatusCode());
	}

	@Test
	void calcularCajonesSinPermiso() {
		URI uri = UriComponentsBuilder
				.fromHttpUrl("http://localhost:" + this.port)
				.path("/meetup/clima-calculos/cervezas")
				.queryParam("participantes", "4")
				.build()
				.toUri();

		ResponseEntity<Map> entity = this
				.restTemplate
				.withBasicAuth("user", "user")
				.getForEntity(uri, Map.class);
		Assert.assertEquals(HttpStatus.FORBIDDEN, entity.getStatusCode());
	}

	@Test
	void calcularCajonesSinUbicacion() {
		URI uri = UriComponentsBuilder
				.fromHttpUrl("http://localhost:" + this.port)
				.path("/meetup/clima-calculos/cervezas")
				.queryParam("participantes", "4")
				.build()
				.toUri();

		Assert.assertFalse(this.restTemplate
				.withBasicAuth("admin", "admin")
				.getForObject(uri,
						ClimaDTO.class)
				.isOk());
	}

	@Test
	void climaClientStrategy(){
		//Carga objeto
		ClimaClient climaClient = new ClimaClient();
		climaClient.setCervezasCajon(6);
		List<TipoClima> tipoClimas = Arrays.asList(new TipoClima(25d,19d,1)
		,new TipoClima(20d,0d,0.75d),new TipoClima(0d,24d,3));
		climaClient.cargarStrategyList(tipoClimas);

		//Clima caluroso
		Clima clima = new Clima("Cordoba,ar", LocalDate.now().toString(),30, 15, 26);
		ClimaContext climaContext = climaClient.getEstrategiaClima(clima);
		int cajones = climaContext.getCajonesCerveza(3);
		Assert.assertEquals(2, cajones);

		//Clima templado
		clima = new Clima("Cordoba,ar", LocalDate.now().toString(),26, 12, 24);
		climaContext = climaClient.getEstrategiaClima(clima);
		cajones = climaContext.getCajonesCerveza(4);
		Assert.assertEquals(1, cajones);

		clima = new Clima("Cordoba,ar", LocalDate.now().toString(),26, 12, 20);
		climaContext = climaClient.getEstrategiaClima(clima);
		cajones = climaContext.getCajonesCerveza(4);
		Assert.assertEquals(1, cajones);

		//Clima frio
		clima = new Clima("Cordoba,ar", LocalDate.now().toString(),25, 15, 19);
		climaContext = climaClient.getEstrategiaClima(clima);
		cajones = climaContext.getCajonesCerveza(4);
		Assert.assertEquals(1, cajones);
	}

	@Test
	void cache(){
		ClimaClient client = new ClimaClient();
		Propiedades propiedades = new Propiedades();
		Map<String,Integer> cache = new HashMap<>();
		cache.put("horas", 12);
		propiedades.setCache(cache);
		client.setPropiedades(propiedades);
		String fecha = "2020-11-11";
		String ubicacion = "cordoba,ar";
		String keyCordoba = client.generateKeyCache("cordoba,ar", fecha);
		client.cargarCache(keyCordoba, new ClimaContext(null, new Clima(ubicacion, fecha, 30, 20, 25)));

		String keyBsAs = client.generateKeyCache("buenos aires,ar", fecha);
		client.cargarCache(keyBsAs, new ClimaContext(null, new Clima(ubicacion, fecha, 30, 20, 25)));

		Assert.assertNotNull(client.buscarEnCache("CORDOBA,AR"+fecha));
		Assert.assertNull(client.buscarEnCache("CóRDOBA,AR"+fecha));
		Assert.assertNull(client.buscarEnCache("cordoba"+fecha));
		Assert.assertNotNull(client.buscarEnCache("BUENOSAIRES,AR"+fecha));
	}

}
