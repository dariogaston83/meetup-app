package santander.rio.meetup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import santander.rio.meetup.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    public Usuario findByNombre(String nombre);
}
