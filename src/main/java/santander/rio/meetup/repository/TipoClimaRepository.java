package santander.rio.meetup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import santander.rio.meetup.model.TipoClima;

public interface TipoClimaRepository extends JpaRepository<TipoClima, Integer>{
}
