package santander.rio.meetup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import santander.rio.meetup.model.Parametrizacion;

public interface ParametrizacionRepository extends JpaRepository<Parametrizacion, Integer> {

    public Parametrizacion findByClave(String clave);
}
