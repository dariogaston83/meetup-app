package santander.rio.meetup.rpc;

import com.jayway.restassured.path.json.JsonPath;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import santander.rio.meetup.excepcion.ComunicacionWsException;
import santander.rio.meetup.excepcion.UbicacionInvalidaException;
import santander.rio.meetup.model.Clima;

import java.text.DecimalFormat;

/**
 * Clase para realizar llamadas a los web services externos para obeneter el clima
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
public class ClimaCalls {

    private ClimaCalls(){

    }

    /**
     * Realiza una llamda al web service de Aerisweather
     * @param ubicacion
     * @param dateStr
     * @return
     * @throws UbicacionInvalidaException si la ubicación es invalida
     * @throws ComunicacionWsException si hubo algún error al comunicarse con el web service
     */
    public static Clima getAerisweatherClima(String ubicacion, String dateStr) throws UbicacionInvalidaException, ComunicacionWsException {
        Clima clima = null;
        try {
            HttpResponse<String> response = Unirest.get("https://aerisweather1.p.rapidapi.com/forecasts/"+ubicacion+"?from="+dateStr+"&to="+dateStr)
                    .header("x-rapidapi-key", "e0d1d05912msh0754196f6cd60f9p188552jsn8c88454ff7b1")
                    .header("x-rapidapi-host", "aerisweather1.p.rapidapi.com")
                    .asString();
            String body = response.getBody();
            boolean sucess = JsonPath.from(body).get("success");
            if(sucess) {
                Number max = JsonPath.from(body).get("response.periods.maxTempC[0][0]");
                Number min = JsonPath.from(body).get("response.periods.minTempC[0][0]");
                Number avg = JsonPath.from(body).get("response.periods.avgTempC[0][0]");
                clima = new Clima(ubicacion, dateStr, max.doubleValue(), min.doubleValue(), doubleFormat(avg.doubleValue()));
            } else if(JsonPath.from(body).get("error.code").equals("invalid_location"))
                throw new UbicacionInvalidaException();

        } catch (UnirestException e) {
            throw new ComunicacionWsException();
        }
        return clima;
    }

    private static double doubleFormat(double numero){
        DecimalFormat newFormat = new DecimalFormat("#.#");
        return  Double.valueOf(newFormat.format(numero).replace(",","."));
    }

}
