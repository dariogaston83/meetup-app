package santander.rio.meetup.model;

import javax.persistence.*;

/**
 * Datos referentes al usuario del sistema
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
@Entity
@Table(name = "USUARIOS")
public class Usuario {
    @Id
    @GeneratedValue(generator = "seq_generator_usuario")
    @TableGenerator(name="seq_generator_usuario", table="SEQUENCES",
            pkColumnName="NAME", valueColumnName="VALUE",
            pkColumnValue="Usuario",
            initialValue=1, allocationSize=1)
    @Column(name = "ID", unique = true,nullable = false )
    private Integer id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "PASS")
    private String pass;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ROL")
    private Rol rol;

    @Column(name = "ACTIVO")
    private boolean activo;

    /**
     * Identificador de Base de datos
     * @return Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * Identificador de Base de datos
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Nombre de usuario
     * @return String
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Nombre de usuario
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Password del usuario
     * @return
     */
    public String getPass() {
        return pass;
    }

    /**
     * Password del usuario
     * @param pass
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * Correo electrónico del usuario
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Correo electrónico del usuario
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Define los accesos de usuario al sistema
     * @return
     */
    public Rol getRol() {
        return rol;
    }

    /**
     * Define los accesos de usuario al sistema
     * @param rol
     */
    public void setRol(Rol rol) {
        this.rol = rol;
    }

    /**
     * Bandera que indica si el usuario esta habilitado
     * @return
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * Bandera que indica si el usuario esta habilitado
     * @param activo
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
