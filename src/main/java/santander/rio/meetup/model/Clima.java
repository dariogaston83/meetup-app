package santander.rio.meetup.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Clase inmutable que representa el clima de un dia
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 *  */
public final class Clima {
    private final String ubicacion;
    private final String fecha;
    private final double maxima;
    private final double minima;
    private final double promedio;
    private final Timestamp timestamp;

    public Clima(String ubicacion, String fecha, double maxima, double minima, double promedio) {
        this.ubicacion = ubicacion;
        this.fecha = fecha;
        this.maxima = maxima;
        this.minima = minima;
        this.promedio = promedio;
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
    }

    public String getUbicacion() {
        return ubicacion;
    }

    /**
     * Fecha del clima formato yyyy-mm-dd
     * @return String
     */
    public String getFecha() {
        return this.fecha;
    }

    /**
     * Temperatura maxima del dia
     * @return double
     */
    public double getMaxima() {
        return maxima;
    }

    /**
     * Temperatura minima del dia
     * @return double
     */
    public double getMinima() {
        return minima;
    }

    /**
     * Temperatura promedio del dia
     * @return double
     */
    public double getPromedio() {
        return promedio;
    }

    /**
     * Marca de tiempo de cuando fue tomado los datos
     * @return double
     */
    public String getTimesTamp(){
        return this.timestamp.toString();
    }

}
