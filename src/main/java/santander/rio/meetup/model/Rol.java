package santander.rio.meetup.model;

/**
 * Tipos de roles en la aplicación
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
*/
public enum Rol {
    ADMIN("admin"), USUARIO("usuario");

    private final String nombre;

    private Rol(String nombre){
        this.nombre = nombre;
    }

    /**
     * Nombre del rol
     * @return String
     */
    public String getNombre() {
        return nombre;
    }
}
