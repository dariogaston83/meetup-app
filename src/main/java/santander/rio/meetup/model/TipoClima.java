package santander.rio.meetup.model;

import javax.persistence.*;

/**
 * Contiene los datos necesarios para calcular el consumo de cajones de cerveza dependiendo la temperatura
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
*/
@Entity
@Table(name = "TIPO_CLIMA")
public class TipoClima {

    @Id
    @GeneratedValue(generator = "seq_generator_tipo_clima")
    @TableGenerator(name="seq_generator_tipo_clima", table="SEQUENCES",
            pkColumnName="NAME", valueColumnName="VALUE",
            pkColumnValue="TipoClima",
            initialValue=1, allocationSize=1)
    @Column(name = "ID", unique = true,nullable = false )
    private Integer id;

    @Column(name = "MAXIMA")
    private double maxima;

    @Column(name = "MINIMA")
    private double minima;

    @Column(name = "CERVEZAS_PERSONA")
    private double cervezasPersona;

    public TipoClima() {
    }

    public TipoClima(Double maxima, Double minima, double cervezaXpersona) {
        this.maxima = maxima;
        this.minima = minima;
        this.cervezasPersona = cervezaXpersona;
    }

    /**
     * Identificador de Base de datos
     * @return Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * Identificador de Base de datos
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Temperatura maxima del clima
     * @return double
     */
    public double getMaxima() {
        return maxima;
    }

    /**
     * Temperatura maxima del clima
     * @param maxima
     */
    public void setMaxima(double maxima) {
        this.maxima = maxima;
    }

    /**
     * Temperatura minima del clima
     * @return double
     */
    public double getMinima() {
        return minima;
    }

    /**
     * Temperatura minima del clima
     * @param minima
     */
    public void setMinima(double minima) {
        this.minima = minima;
    }

    /**
     * Cantidad de cervezas consumidas por persona
     * @return
     */
    public double getCervezasPersona() {
        return cervezasPersona;
    }

    /**
     * Cantidad de cervezas consumidas por persona
     * @param cervezasPersona
     */
    public void setCervezasPersona(double cervezasPersona) {
        this.cervezasPersona = cervezasPersona;
    }
}
