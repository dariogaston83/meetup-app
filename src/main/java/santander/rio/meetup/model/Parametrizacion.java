package santander.rio.meetup.model;

import javax.persistence.*;

/**
 * Representa las parametrizaciones del sistema
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 *  */
@Entity
@Table(name = "PARAMETRIZACION")
public class Parametrizacion {

    @Id
    @GeneratedValue(generator = "seq_generator_parametrizacion")
    @TableGenerator(name="seq_generator_parametrizacion", table="SEQUENCES",
            pkColumnName="NAME", valueColumnName="VALUE",
            pkColumnValue="Parametrizacion",
            initialValue=1, allocationSize=1)
    @Column(name = "ID", unique = true,nullable = false )
    private Integer id;

    /**
     * Valor para recuperar el parámetro
     */
    @Column(name = "CLAVE")
    private String clave;

    /**
     * Valor de la parametrización
     */
    @Column(name = "VALOR")
    private String valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
