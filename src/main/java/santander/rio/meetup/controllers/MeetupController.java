package santander.rio.meetup.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import santander.rio.meetup.dto.CalculoCervezasDTO;
import santander.rio.meetup.dto.RespuestaDTO;
import santander.rio.meetup.excepcion.ComunicacionWsException;
import santander.rio.meetup.excepcion.UbicacionInvalidaException;
import santander.rio.meetup.model.Clima;
import santander.rio.meetup.dto.ClimaDTO;
import santander.rio.meetup.strategy.ClimaClient;
import santander.rio.meetup.strategy.ClimaContext;
import santander.rio.meetup.util.Formatters;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Controlador de servicio web, contiene las llamadas a funcionalidad de meetup
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
@RestController
@RequestMapping("/meetup")
@Api(value = "Meetup servicio")
public class MeetupController {

    private static final  String MENSAJE_ERROR_FECHA = "No se reconoce la fecha el formato requerido es yyyy-mm-dd, fecha: ";
    private static final String MENSAJE_ERROR_UBICACION = "La ubicacón indicada no es valida, ubicación: ";

    private ClimaClient climaClient;

    @Autowired
    public void setClimaClient(ClimaClient climaClient) {
        this.climaClient = climaClient;
    }

    /**
     * Retorna el clima para una ubicación y fecha dada, si no se indica fecha se tomara la actual
     * @param ubicacion String formato: ubicación, país
     * @param fecha String formato: yyyy-mm-dd
     * @return RespuestaDTO representa los datos del clima
     */
    @GetMapping("/clima")
    @ApiOperation(value = "Devuelve el clima del día indicado", notes = "Si no se indica fecha se utiliza la fecha del día actual" )
    public RespuestaDTO getClima(@RequestParam(defaultValue="") String ubicacion, @RequestParam(defaultValue="") String fecha){
        return getClimaDTO(ubicacion, fecha);
    }

    /**
     * Retorna el calculo de la cantidad de cajones de cerveza necesarios para la fecha indicada segun la temperatura del día
     * @param ubicacion String formato: ubicación, país
     * @param fecha String formato: yyyy-mm-dd
     * @param participantes int
     * @return  RespuestaDTO representa los datos del calculo de los cajones de cervezas necesarios para la meetup y datos del clima para la fecha indicada
     */
    @GetMapping("/clima-calculos/cervezas")
    @ApiOperation(value = "Devuelve la cantidad de cajones de cerveza que se deben comprar para el día y la cantidad de participantes"
            , notes = "Si no se indica fecha se utiliza la fecha del día actual\n Si no se indica número de participantes por defecto es 0")
    public RespuestaDTO calcularCajones(@RequestParam(defaultValue="") String ubicacion, @RequestParam(defaultValue="") String fecha, @RequestParam(defaultValue="0") int participantes){
        return getCalculoCervezasDTO(ubicacion, fecha, participantes);
    }

    private RespuestaDTO getCalculoCervezasDTO(String ubicacion, String fecha, int participantes) {
        CalculoCervezasDTO calculoCervezasDTO = new CalculoCervezasDTO(true, null);
        try {
            esUbicacionValida(ubicacion);
            LocalDate date = fecha.equals("") ? LocalDate.now() : LocalDate.parse(fecha, Formatters.DATE_FORMATTER);
            ClimaContext climaContext = climaClient.getClimaContext(ubicacion, date);
            calculoCervezasDTO.setCajones(climaContext.getCajonesCerveza(participantes));
            calculoCervezasDTO.setClima(climaContext.getClima());
            calculoCervezasDTO.setFecha(fecha);
        }catch(DateTimeParseException e){
            calculoCervezasDTO.setError(MENSAJE_ERROR_FECHA+fecha);
            calculoCervezasDTO.setOk(false);
        } catch (UbicacionInvalidaException e) {
            calculoCervezasDTO.setOk(false);
            calculoCervezasDTO.setError(MENSAJE_ERROR_UBICACION+ubicacion);
        } catch (ComunicacionWsException e) {
            e.printStackTrace();
        }
        return calculoCervezasDTO;
    }

    private RespuestaDTO getClimaDTO(String ubicacion, String fecha) {
        ClimaDTO climaDTO  = new ClimaDTO(true, null);
        try {
            esUbicacionValida(ubicacion);
            LocalDate date = fecha.equals("") ? LocalDate.now() : LocalDate.parse(fecha, Formatters.DATE_FORMATTER);
            Clima clima = getClima(ubicacion, date);
            climaDTO.setClima(clima);
        }catch(DateTimeParseException e){
            climaDTO.setOk(false);
            climaDTO.setError(MENSAJE_ERROR_FECHA+fecha);
        } catch (UbicacionInvalidaException e) {
            climaDTO.setOk(false);
            climaDTO.setError(MENSAJE_ERROR_UBICACION+ubicacion);
        } catch (ComunicacionWsException e) {
            e.printStackTrace();
        }
        return climaDTO;
    }

    private void esUbicacionValida(@RequestParam(defaultValue = "") String ubicacion) throws UbicacionInvalidaException {
        if (ubicacion.equals("")) throw new UbicacionInvalidaException();
    }

    private Clima getClima(String ubicacion, LocalDate date) throws UbicacionInvalidaException, ComunicacionWsException {
        ClimaContext climaContext = climaClient.getClimaContext(ubicacion, date);
        return climaContext.getClima();
    }
}
