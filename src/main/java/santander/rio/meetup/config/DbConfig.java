package santander.rio.meetup.config;

import com.mchange.v2.c3p0.DataSources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Configuración de la Base de Datos de la aplicación
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
@Configuration
@EnableJpaRepositories(basePackages = "santander.rio.meetup.repository")
@PropertySource(value = {"persistence-sqlite.properties"})
public class DbConfig {

    public static final String HIBERNATE_HBM_2_DDL_AUTO = "hibernate.hbm2ddl.auto";
    public static final String HIBERNATE_DIALECT = "hibernate.dialect";
    public static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSourceSqlLite() throws SQLException {
        // configure SQLite
        SQLiteConfig config = new org.sqlite.SQLiteConfig();
        config.setReadOnly(false);
        config.setPageSize(4096); //in bytes
        config.setCacheSize(2000); //number of pages
        config.setSynchronous(SQLiteConfig.SynchronousMode.FULL);
        config.setJournalMode(SQLiteConfig.JournalMode.OFF);

        // get an unpooled SQLite DataSource with the desired configuration
        SQLiteDataSource unpooled = new SQLiteDataSource( config );
        unpooled.setUrl(env.getProperty("url"));

        // get a pooled c3p0 DataSource that wraps the unpooled SQLite DataSource
        return DataSources.pooledDataSource( unpooled );

    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws SQLException {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSourceSqlLite());
        em.setPackagesToScan("santander.rio.meetup.model");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(additionalProperties());
        return em;
    }

    final Properties additionalProperties() {
        final Properties hibernateProperties = new Properties();
        if (env.getProperty(HIBERNATE_HBM_2_DDL_AUTO) != null) {
            hibernateProperties.setProperty(HIBERNATE_HBM_2_DDL_AUTO, env.getProperty(HIBERNATE_HBM_2_DDL_AUTO));
        }
        if (env.getProperty(HIBERNATE_DIALECT) != null) {
            hibernateProperties.setProperty(HIBERNATE_DIALECT, env.getProperty(HIBERNATE_DIALECT));
        }
        if (env.getProperty(HIBERNATE_SHOW_SQL) != null) {
            hibernateProperties.setProperty(HIBERNATE_SHOW_SQL, env.getProperty(HIBERNATE_SHOW_SQL));
        }
        return hibernateProperties;
    }

}

