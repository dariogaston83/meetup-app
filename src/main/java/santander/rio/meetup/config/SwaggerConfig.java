package santander.rio.meetup.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * Configuracion de Swagger
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Docket de configuración
     * @return Docket
     */
    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("santander.rio.meetup.controllers"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    /**
     * Inforamción que se mostrara sobre la apliación
     * @return ApiInfo
     */
    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "Meetup Service API",
                "Api para poder gestionar meetups",
                "1.0",
                "",
                new Contact("Dario", "https://www.linkedin.com/in/dario-gaston-7348a941/", "dariogaston83@gmail.com"),
                "",
                "",
                Collections.emptyList()
        );
    }
}
