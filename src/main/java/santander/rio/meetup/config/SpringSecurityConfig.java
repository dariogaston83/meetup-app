package santander.rio.meetup.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import santander.rio.meetup.model.Rol;

/**
 * Implementación para la seguridad web de la aplicación
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationProvider authProvider;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authProvider);
    }

    /**
     * Configura la seguridad que tendran las llamdas web
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
       http.httpBasic().and().authorizeRequests().antMatchers("/meetup/clima-calculos/**").hasRole(Rol.ADMIN.getNombre())
               .antMatchers("/meetup/clima/**").hasAnyRole(Rol.ADMIN.getNombre(), Rol.USUARIO.getNombre()).and()
                .csrf().disable();
    }


}
