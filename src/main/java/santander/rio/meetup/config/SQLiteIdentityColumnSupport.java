package santander.rio.meetup.config;

import org.hibernate.dialect.identity.IdentityColumnSupportImpl;

/**
 * Implementación para SQLite de la columna de identificación de la Base de Datos
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
public class SQLiteIdentityColumnSupport extends IdentityColumnSupportImpl {

    @Override
    public boolean supportsIdentityColumns() {
        return true;
    }

    @Override
    public String getIdentitySelectString(String table, String column, int type)  {
        return "select last_insert_rowid()";
    }

    @Override
    public String getIdentityColumnString(int type) {
        return "integer";
    }
}