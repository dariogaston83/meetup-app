package santander.rio.meetup.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import santander.rio.meetup.model.Usuario;
import santander.rio.meetup.repository.UsuarioRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementación de la lógica de autentificación de la aplicación
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UsuarioRepository usuarioRepository;

    /**
     * Permite autentificar las credenciales ingresadas
     * @param authentication
     * @return Authentication
     */
    @Override
    public Authentication authenticate(Authentication authentication) {

        String nombre = authentication.getName();
        String pass = authentication.getCredentials().toString();

        Usuario usuario = usuarioRepository.findByNombre(nombre);
        if(usuario == null || !usuario.getPass().equals(pass))
            throw new BadCredentialsException("Acceso denegado");
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_"+usuario.getRol().getNombre())); // description is a string

        return new UsernamePasswordAuthenticationToken(nombre, pass, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}