package santander.rio.meetup.dto;

/**
 * Respuesta de los servicios web
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
public class RespuestaDTO {
    private boolean ok;
    private String error;

    public RespuestaDTO() {
    }

    /**
     * Constructor
     * @param ok
     * @param error
     */
    public RespuestaDTO(boolean ok, String error) {
        this.ok = ok;
        this.error = error;
    }

    /**
     * Indica si la petición se pudo procesar correctamente
     * @return boolean
     */
    public boolean isOk() {
        return ok;
    }

    /**
     * Indica si la petición se pudo procesar correctamente
     * @param ok
     */
    public void setOk(boolean ok) {
        this.ok = ok;
    }

    /**
     * Devuelve la descripción de error si es que hubo
     * @return String or null
     */
    public String getError() {
        return error;
    }

    /**
     * Devuelve la descripción de error si es que hubo
     * @param error
     */
    public void setError(String error) {
        this.error = error;
    }

}
