package santander.rio.meetup.dto;

import santander.rio.meetup.model.Clima;

/**
 * Respuesta que contiene el calculo de la cantidad de cajones de cerveza necesarios para una meetup y
 * el clima de ese día con el cual se uso para realizar el cálculo
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
public class CalculoCervezasDTO extends RespuestaDTO{
    private int cajones;
    private String fecha;
    private Clima clima;

    public CalculoCervezasDTO() {
    }

    /**
     * Constructor
     * @param ok
     * @param error
     */
    public CalculoCervezasDTO(boolean ok, String error) {
        super(ok, error);
    }

    /**
     * Cantidad de cajonnes necesarios para la meetup
     * @return int
     */
    public int getCajones() {
        return cajones;
    }

    /**
     * Cantidad de cajonnes necesarios para la meetup
     * @param cajones
     */
    public void setCajones(int cajones) {
        this.cajones = cajones;
    }

    /**
     * Fecha de la meetup, formato yyyy-mm-dd
     * @return String
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Fecha de la meetup, formato yyyy-mm-dd
     * @param fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Clima de la meetup
     * @return Clima
     */
    public Clima getClima() {
        return clima;
    }

    /**
     * Clima de la meetup
     * @param clima
     */
    public void setClima(Clima clima) {
        this.clima = clima;
    }
}
