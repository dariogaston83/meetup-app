package santander.rio.meetup.dto;

import santander.rio.meetup.model.Clima;

/**
 * Respuesta que contiene el clima de un día indicado
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
public class ClimaDTO extends RespuestaDTO{
    private Clima clima;

    public ClimaDTO() {
    }

    /**
     * Constructor
     * @param ok
     * @param error
     */
    public ClimaDTO(boolean ok, String error) {
        super(ok, error);
    }

    public ClimaDTO(boolean ok, String error, Clima clima) {
        super(ok, error);
        this.clima = clima;
    }

    /**
     * Devuelve el clima con sus datos
     * @return Clima
     */
    public Clima getClima() {
        return clima;
    }

    /**
     * Carga un clima al objeto
     * @param clima
     */
    public void setClima(Clima clima) {
        this.clima = clima;
    }
}
