package santander.rio.meetup.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "app")
public class Propiedades {

    private Map<String,Integer> cache;

    public void setCache(Map<String, Integer> cache) {
        this.cache = cache;
    }

    /**
     * Cantidad de horas de vida de la cache
     * @return int
     */
    public int getRefrescoCacheHoras(){
        Integer horas = cache.get("horas");
        return horas != null ? horas : 0;
    }

    /**
     * Cantidad de minutos de vida de la cache
     * @return int
     */
    public int getRefrescoCacheMinutos(){
        Integer horas = cache.get("minutos");
        return horas != null ? horas : 0;
    }

}
