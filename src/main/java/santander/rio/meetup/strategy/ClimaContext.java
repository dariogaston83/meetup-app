package santander.rio.meetup.strategy;

import santander.rio.meetup.model.Clima;

/**
 * Clase inmutable que representa el contexto del patron Strategy para el calculo de consumos dependiendo el clima
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 */
public final class ClimaContext {
    private final ClimaStrategy climaStrategy;
    private final Clima clima;

    public ClimaContext(ClimaStrategy climaStrategy, Clima clima) {
        this.climaStrategy = climaStrategy;
        this.clima = clima;
    }

    public int getCajonesCerveza(int personas){
        return climaStrategy.getCajonesCerveza(personas);
    }

    public Clima getClima() {
        return clima;
    }

}
