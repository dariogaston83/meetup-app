package santander.rio.meetup.strategy;

/**
 * Representa la estrategia a utilizar para el calculo de cajones de cerveza necesarios para una meetup
 */
@FunctionalInterface
public interface ClimaStrategy {
    int getCajonesCerveza(int personas);
}
