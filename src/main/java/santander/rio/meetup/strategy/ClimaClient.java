package santander.rio.meetup.strategy;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import santander.rio.meetup.excepcion.ComunicacionWsException;
import santander.rio.meetup.excepcion.UbicacionInvalidaException;
import santander.rio.meetup.model.Clima;
import santander.rio.meetup.model.TipoClima;
import santander.rio.meetup.repository.ParametrizacionRepository;
import santander.rio.meetup.repository.TipoClimaRepository;
import santander.rio.meetup.rpc.ClimaCalls;
import santander.rio.meetup.util.Formatters;
import santander.rio.meetup.util.Propiedades;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Representa el Cliente en el patron Strategy para obtener el Contexto indicado segun las variables de temperatura del día.
 * El Clima de un dia dado es guardado en una cache interna, el tiempo de refresco se indica en el parámetro
 * "app.cache.horas" y  "app.cache.minutos" del sistema.
 * @author Gaston, Dario
 * @version 1.0
 * @since 2020/11/12
 *  */
@Component
public class ClimaClient implements InitializingBean {

    private Propiedades propiedades;

    private TipoClimaRepository tipoClimaRepository;

    private ParametrizacionRepository parametrizacionRepository;

    private final List<Strategies> strategiesList;

    private int cervezasCajon;

    private final HashMap<String,ClimaContext> cache = new HashMap<>();

    public ClimaClient() {
        strategiesList = new ArrayList<>();
    }

    @Autowired
    public void setPropiedades(Propiedades propiedades) {
        this.propiedades = propiedades;
    }

    @Autowired
    public void setTipoClimaRepository(TipoClimaRepository tipoClimaRepository) {
        this.tipoClimaRepository = tipoClimaRepository;
    }

    @Autowired
    public void setParametrizacionRepository(ParametrizacionRepository parametrizacionRepository) {
        this.parametrizacionRepository = parametrizacionRepository;
    }

    public int getCervezasCajon() {
        return cervezasCajon;
    }

    public void setCervezasCajon(int cervezasCajon) {
        this.cervezasCajon = cervezasCajon;
    }

    /**
     * Retorna el Contexto de patrón strategy
     * @param ubicacion String
     * @param date LocalDate
     * @return ClimaContext
     * @throws UbicacionInvalidaException si la ubicación es invalida
     * @throws ComunicacionWsException si hubo algún error al comunicarse con el web service
     */
    public ClimaContext getClimaContext(String ubicacion, LocalDate date) throws UbicacionInvalidaException, ComunicacionWsException {
        String dateStr = date.format(Formatters.DATE_FORMATTER);
        String keyCache = generateKeyCache(ubicacion, dateStr);
        ClimaContext climaContext = buscarEnCache(keyCache);
        if(climaContext == null){
            Clima clima = ClimaCalls.getAerisweatherClima(ubicacion, dateStr);
            climaContext = getEstrategiaClima(clima);
            cargarCache(keyCache, climaContext);
        }
        return climaContext;
        }

    /**
     * Devuelve la estrategia segun la temperatua promedio, sino existe ninguna estrategia para el clima indicado devuelve lanza un NullPointerException
     * @param clima Clima
     * @return ClimaContext
     */
    public ClimaContext getEstrategiaClima(Clima clima) {
        double temp = clima.getPromedio();

        Optional<Strategies> optional = strategiesList
                .stream()
                .filter(st -> st
                        .getMatch()
                        .test(temp))
                .findFirst();

        if(!optional.isPresent())
            throw new NullPointerException("No se han cargado estrategias de calculo de clima");

        Strategies strategy = optional.get();
        return new ClimaContext(strategy.getStrategy(), clima);
    }

    /**
     * Devuelve el ClimaContext si existe en la cache
     * @param dateKey String
     * @return ClimaContext
     */
    public ClimaContext buscarEnCache(String dateKey){
        ClimaContext climaContext = cache.get(dateKey);
        if(climaContext != null && renovarCache(climaContext.getClima()))
            climaContext = null;
        return climaContext;
    }

    /**
     * Carga un nuevo ClimaContext en la cache
     * @param key String
     * @param value ClimaContext
     */
    public void cargarCache(String key, ClimaContext value){
        cache.put(key, value);
    }

    /**
     * Genera una key para utilizar dentro de la cache
     * @param ubicacion String
     * @param fecha String
     * @return String
     */
    public String generateKeyCache(String ubicacion, String fecha){
        return ubicacion.replace(" ","").toUpperCase()+fecha;
    }

    private boolean renovarCache(Clima clima) {
        Timestamp timestampClima = Timestamp.valueOf(clima.getTimesTamp());
        LocalDateTime horaComparar  = timestampClima.toLocalDateTime().plusHours(propiedades.getRefrescoCacheHoras()).plusMinutes(propiedades.getRefrescoCacheMinutos());
        return LocalDateTime.now().isAfter(horaComparar) || LocalDateTime.now().isEqual(horaComparar);
    }

    /**
     * Carga la lista de estrategias persisitas en la base de datos
     * @param tipoClimas
     */
    public void cargarStrategyList(List<TipoClima> tipoClimas){
        for (TipoClima tipoClima : tipoClimas){
            Predicate<Double> predicate = tmp -> esMatch(tipoClima,tmp);
            ClimaStrategy climaStrategy = personas -> getCajonesCerveza(personas, tipoClima.getCervezasPersona());
            strategiesList.add(new Strategies(predicate, climaStrategy));
        }
    }

    private boolean esMatch(TipoClima tipoClima, double temperatura){
        double max = tipoClima.getMaxima();
        double min = tipoClima.getMinima();
        boolean flagMax = tipoClima.getMaxima() > 0 ? (temperatura < max) : true;
        boolean flagMin = tipoClima.getMinima() > 0 ? (temperatura > min) : true;
        return flagMax && flagMin;
    }

    private int getCajonesCerveza(int personas, double consumo) {
        double cervezas = consumo * personas;
        BigDecimal cajones = BigDecimal.valueOf(cervezas / cervezasCajon);
        return cajones.setScale(0, RoundingMode.HALF_UP).intValue();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
         cargarStrategyList(tipoClimaRepository.findAll());
         cervezasCajon = Integer.valueOf(parametrizacionRepository.findByClave("CERVEZAS_CAJON").getValor());
    }

    private static class Strategies{
        private Predicate<Double> match;
        private ClimaStrategy strategy;

        public Strategies(Predicate<Double> match, ClimaStrategy strategy) {
            this.match = match;
            this.strategy = strategy;
        }

        public Predicate<Double> getMatch() {
            return match;
        }

        public ClimaStrategy getStrategy() {
            return strategy;
        }
    }

}
